package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyHandler implements KeyboardHandler {

    public boolean up, down, left, right;
    public boolean painting, clearing, cleaning;
    public boolean growing, shrinking;
    public boolean saving, loading;
    public boolean speedingUp, speedingDown;

    public boolean blue, magenta, yellow, green, red, black;

    public boolean randomColor;

    public boolean fill, redo;
    private Keyboard keyboard;
    private Grid grid;
    private KeyboardEvent[] keyboardEvents;
    private int[] eventKeys;

    public KeyHandler(Grid grid) {
            keyboard = new Keyboard(this);
            keyboardEvents = new KeyboardEvent[23];
            eventKeys = new int[keyboardEvents.length];
            this.grid = grid;
            createEvents();
            black = true;
    }

    public void createEvents() {
        for(int i = 0; i < keyboardEvents.length; i++) {
            keyboardEvents[i] = new KeyboardEvent();
        }

        keyboardEvents[0].setKey(KeyboardEvent.KEY_W); // MOVING UP
        eventKeys[0] = KeyboardEvent.KEY_W;
        keyboardEvents[1].setKey(KeyboardEvent.KEY_S); // MOVING DOWN
        eventKeys[1] = KeyboardEvent.KEY_S;
        keyboardEvents[2].setKey(KeyboardEvent.KEY_A); // MOVING LEFT
        eventKeys[2] = KeyboardEvent.KEY_A;
        keyboardEvents[3].setKey(KeyboardEvent.KEY_D); // MOVING RIGHT
        eventKeys[3] = KeyboardEvent.KEY_D;
        keyboardEvents[4].setKey(KeyboardEvent.KEY_SPACE); // PAINTING MODE
        eventKeys[4] = KeyboardEvent.KEY_SPACE;
        keyboardEvents[5].setKey(KeyboardEvent.KEY_C); // CLEAR MODE
        eventKeys[5] = KeyboardEvent.KEY_C;
        keyboardEvents[6].setKey(KeyboardEvent.KEY_Z); // GROW
        eventKeys[6] = KeyboardEvent.KEY_Z;
        keyboardEvents[7].setKey(KeyboardEvent.KEY_X); // SHRINK
        eventKeys[7] = KeyboardEvent.KEY_X;
        keyboardEvents[8].setKey(KeyboardEvent.KEY_K); // SAVE
        eventKeys[8] = KeyboardEvent.KEY_K;
        keyboardEvents[9].setKey(KeyboardEvent.KEY_L); // LOAD
        eventKeys[9] = KeyboardEvent.KEY_L;
        keyboardEvents[10].setKey(KeyboardEvent.KEY_O); // SAVE
        eventKeys[10] = KeyboardEvent.KEY_O;
        keyboardEvents[11].setKey(KeyboardEvent.KEY_I); // LOAD
        eventKeys[11] = KeyboardEvent.KEY_I;
        keyboardEvents[12].setKey(KeyboardEvent.KEY_R); // LOAD
        eventKeys[12] = KeyboardEvent.KEY_R;
        keyboardEvents[13].setKey(KeyboardEvent.KEY_1); // Paint Blue
        eventKeys[13] = KeyboardEvent.KEY_1;
        keyboardEvents[14].setKey(KeyboardEvent.KEY_2); // Paint Magenta
        eventKeys[14] = KeyboardEvent.KEY_2;
        keyboardEvents[15].setKey(KeyboardEvent.KEY_3); // Paint Yellow
        eventKeys[15] = KeyboardEvent.KEY_3;
        keyboardEvents[16].setKey(KeyboardEvent.KEY_4); // Paint Green
        eventKeys[16] = KeyboardEvent.KEY_4;
        keyboardEvents[17].setKey(KeyboardEvent.KEY_5); // Paint Red
        eventKeys[17] = KeyboardEvent.KEY_5;
        keyboardEvents[18].setKey(KeyboardEvent.KEY_0); // Paint Black
        eventKeys[18] = KeyboardEvent.KEY_0;
        keyboardEvents[19].setKey(27); // EXIT PROGRAM
        eventKeys[19] = 27;
        keyboardEvents[20].setKey(KeyboardEvent.KEY_F); // FILL
        eventKeys[20] = KeyboardEvent.KEY_F;
        keyboardEvents[21].setKey(KeyboardEvent.KEY_B); // DELETE
        eventKeys[21] = KeyboardEvent.KEY_B;
        keyboardEvents[22].setKey(KeyboardEvent.KEY_M); // DELETE
        eventKeys[22] = KeyboardEvent.KEY_M;



        for(int i = 0; i < keyboardEvents.length; i++) {
            keyboardEvents[i].setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(keyboardEvents[i]);
        }

        for(int i = 0; i < keyboardEvents.length; i++) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
            event.setKey(keyboardEvents[i].getKey());
            keyboard.addEventListener(event);
        }

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if(keyboardEvent.getKey() == eventKeys[0]) {
            up = true;
        }

        if(keyboardEvent.getKey() == eventKeys[1]) {
            down = true;
        }

        if(keyboardEvent.getKey() == eventKeys[2]) {
            left = true;
        }

        if(keyboardEvent.getKey() == eventKeys[3]) {
            right = true;
        }

        if(keyboardEvent.getKey() == eventKeys[4]) {
            resetPaint();
            painting = true;
        }

        if(keyboardEvent.getKey() == eventKeys[5]) {
            clearing = true;
        }

        if(keyboardEvent.getKey() == eventKeys[6]) {
            growing = true;
        }

        if(keyboardEvent.getKey() == eventKeys[7]) {
            shrinking = true;
        }

        if(keyboardEvent.getKey() == eventKeys[8]) {
            saving = true;
        }

        if(keyboardEvent.getKey() == eventKeys[9]) {
            loading = true;
        }

        if(keyboardEvent.getKey() == eventKeys[10]) {
            speedingUp = true;
        }

        if(keyboardEvent.getKey() == eventKeys[11]) {
            speedingDown = true;
        }

        if(keyboardEvent.getKey() == eventKeys[12]) {
            resetColors();
            randomColor = true;
        }

        if(keyboardEvent.getKey() == eventKeys[13]) {
            resetColors();
            blue = true;
        }

        if(keyboardEvent.getKey() == eventKeys[14]) {
            resetColors();
            magenta = true;
        }

        if(keyboardEvent.getKey() == eventKeys[15]) {
            resetColors();
            yellow = true;
        }

        if(keyboardEvent.getKey() == eventKeys[16]) {
            resetColors();
            green = true;
        }

        if(keyboardEvent.getKey() == eventKeys[17]) {
            resetColors();
            red = true;
        }

        if(keyboardEvent.getKey() == eventKeys[18]) {
            resetColors();
            black = true;
        }

        if(keyboardEvent.getKey() == eventKeys[19]) {
            System.exit(0);
        }
        if(keyboardEvent.getKey() == eventKeys[20]) {
            fill = true;
        }
        if(keyboardEvent.getKey() == eventKeys[21]) {
            resetPaint();
            cleaning = true;
        }
        if(keyboardEvent.getKey() == eventKeys[22]) {
            resetPaint();
        }




    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

        if(keyboardEvent.getKey() == keyboardEvents[0].getKey()) up = false;
         if(keyboardEvent.getKey() == keyboardEvents[1].getKey()) down = false;
         if(keyboardEvent.getKey() == keyboardEvents[2].getKey()) left = false;
         if(keyboardEvent.getKey() == keyboardEvents[3].getKey()) right = false;

        //if(keyboardEvent.getKey() == keyboardEvents[4].getKey()) painting = false;
        if(keyboardEvent.getKey() == keyboardEvents[5].getKey()) clearing = false;

        if(keyboardEvent.getKey() == keyboardEvents[6].getKey()) growing = false;
        if(keyboardEvent.getKey() == keyboardEvents[7].getKey()) shrinking = false;

        if(keyboardEvent.getKey() == keyboardEvents[8].getKey()) saving = false;
        if(keyboardEvent.getKey() == keyboardEvents[9].getKey()) loading = false;

        if(keyboardEvent.getKey() == keyboardEvents[10].getKey()) speedingUp = false;
        if(keyboardEvent.getKey() == keyboardEvents[11].getKey()) speedingDown = false;

        if(keyboardEvent.getKey() == keyboardEvents[20].getKey()) fill = false;
        //if(keyboardEvent.getKey() == keyboardEvents[21].getKey()) cleaning = false;
    }

    public void resetColors() {
        blue = false;
        magenta = false;
        yellow = false;
        green = false;
        red = false;
        black = false;
        randomColor = false;
    }

    public void resetPaint() {
        painting = false;
        cleaning = false;
    }
}
