package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.w3c.dom.Text;

public class Field {

    public static final int TILE_SIZE = 10;
    public static final int PADDING = 10;
    public static int MAX_COLS;
    public static int MAX_ROWS;

    public static int SCREEN_HEIGHT;
    public static int SCREEN_WIDTH;
    public static Picture PICTURE;

    public static int INFO_PANEL_X;
    public static int INFO_PANEL_Y;
    public static int INFO_PANEL_WIDTH;
    public static int INFO_PANEL_HEIGHT;

    public static void generateValues(String path) {
        PICTURE = new Picture(Field.PADDING, Field.PADDING, path);
        MAX_COLS = PICTURE.getWidth()/TILE_SIZE;
        MAX_ROWS = PICTURE.getHeight()/TILE_SIZE;
        SCREEN_HEIGHT = PICTURE.getHeight() + PADDING;
        SCREEN_WIDTH = PICTURE.getWidth() + PADDING;
        INFO_PANEL_X = SCREEN_WIDTH + PADDING;
        INFO_PANEL_Y = PADDING;
        INFO_PANEL_WIDTH = TILE_SIZE * 15;
        INFO_PANEL_HEIGHT = SCREEN_HEIGHT;
        PICTURE.draw();
    }

    public static void generateValues(int cols, int rows) {
        MAX_COLS = cols;
        MAX_ROWS = rows;
        SCREEN_HEIGHT = MAX_ROWS*TILE_SIZE + PADDING;
        SCREEN_WIDTH = MAX_COLS*TILE_SIZE + PADDING;
        INFO_PANEL_X = SCREEN_WIDTH + PADDING;
        INFO_PANEL_Y = PADDING;
        INFO_PANEL_WIDTH = TILE_SIZE * 15;
        INFO_PANEL_HEIGHT = SCREEN_HEIGHT;
    }

    public static void generateValues() {
        MAX_COLS = 50;
        MAX_ROWS = 50;
        SCREEN_HEIGHT = MAX_ROWS*TILE_SIZE + PADDING;
        SCREEN_WIDTH = MAX_COLS*TILE_SIZE + PADDING;
        INFO_PANEL_X = SCREEN_WIDTH + PADDING;
        INFO_PANEL_Y = PADDING;
        INFO_PANEL_WIDTH = TILE_SIZE * 15;
        INFO_PANEL_HEIGHT = SCREEN_HEIGHT;
    }


}
