package org.academiadecodigo.Main;

public enum Direction {

    UP,
    DOWN,
    LEFT,
    RIGHT
}
