package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Game implements Runnable{

    private Grid grid;
    private PaintBrush paintBrush;
    private InfoPanel infoPanel;
    int gameSpeed;



    public Game() {
        Field.generateValues("gladiator_arena_map.png");
        grid = new Grid();
        paintBrush = new PaintBrush(grid);
        grid.setPaintBrush(paintBrush);
        grid.setKeyHandler(paintBrush.keyHandler);
        gameSpeed = 100;
    }

    public void startGame() {
        grid.createTiles();
        paintBrush.load();
        infoPanel = new InfoPanel();
        run();
    }

    @Override
    public void run() {

        while(true) {
            update();

            if(paintBrush.keyHandler.clearing) {
                grid.clearScreen();
                paintBrush.keyHandler.clearing = false;
            }
            checkSpeed();

            //paintBrush.brush.fill();

            try {
                Thread.sleep(gameSpeed);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void checkSpeed() {
        if(paintBrush.keyHandler.speedingUp) {
            gameSpeed = gameSpeed - 5 < 20 ? gameSpeed : gameSpeed - 5;
        }
        if(paintBrush.keyHandler.speedingDown) {
            gameSpeed = gameSpeed + 5 > 200 ? gameSpeed : gameSpeed +5;
        }
    }

    public void update() {
        grid.update();
        paintBrush.update();
    }

}
