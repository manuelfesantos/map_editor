package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class StartMenu {

    private Rectangle backGround;

    public StartMenu() {
        backGround = new Rectangle(Field.PADDING, Field.PADDING, Field.SCREEN_WIDTH, Field.SCREEN_HEIGHT);
        backGround.setColor(Color.BLACK);
        backGround.fill();
    }
}
