package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class PaintBrush {

    Rectangle brush;
    Grid grid;
    KeyHandler keyHandler;


    public PaintBrush(Grid grid) {
        brush = new Rectangle(Field.PADDING, Field.PADDING, Field.TILE_SIZE, Field.TILE_SIZE);
        this.grid = grid;
        keyHandler = new KeyHandler(grid);
    }

    public void load() {
        brush.setColor(Color.GREEN);
        brush.fill();
    }

    public void update() {
        if(keyHandler.up) {
            brush.translate(0, brush.getY() <= Field.PADDING ? 0 : -Field.TILE_SIZE);
        } if(keyHandler.down) {
            brush.translate(0, brush.getY()+brush.getHeight() >= Field.SCREEN_HEIGHT ? 0 : Field.TILE_SIZE);
        } if(keyHandler.left) {
            brush.translate(brush.getX() <= Field.PADDING ? 0 : -Field.TILE_SIZE, 0);
        } if(keyHandler.right) {
            brush.translate(brush.getX()+brush.getWidth() >= Field.SCREEN_WIDTH ? 0 : Field.TILE_SIZE, 0);
        }

        brush.setColor(Color.GREEN);
        /*
        if(keyHandler.black) brush.setColor(Color.BLACK);
        if(keyHandler.blue) brush.setColor(Color.BLUE);
        if(keyHandler.magenta) brush.setColor(Color.MAGENTA);
        if(keyHandler.yellow) brush.setColor(Color.YELLOW);
        if(keyHandler.green) brush.setColor(Color.GREEN);
        if(keyHandler.red) brush.setColor(Color.RED);
        if(keyHandler.randomColor) brush.setColor(ColorType.BLACK.getRandomColor());

         */

        if(keyHandler.growing) {
            if((brush.getX() > Field.PADDING && brush.getX()+brush.getWidth() < Field.SCREEN_WIDTH) &&
            (brush.getY() > Field.PADDING && brush.getY()+brush.getHeight() < Field.SCREEN_HEIGHT)) {

                brush.grow(Field.TILE_SIZE, Field.TILE_SIZE);
            }

        }
        if(keyHandler.shrinking) {
            brush.grow((brush.getWidth() <= Field.TILE_SIZE) ? 0 : -Field.TILE_SIZE,(brush.getHeight() <= Field.TILE_SIZE) ? 0 :  -Field.TILE_SIZE);
        }
        brush.fill();
    }



}
