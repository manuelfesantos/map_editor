package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Grid {

    private Tile[][] tiles;
    private PaintBrush paintBrush;
    private KeyHandler keyHandler;
    private List<Tile> tileList;
    private List<Tile> paintList;
    private List<Tile> redoList;


    public Grid() {
        tileList = new ArrayList<>();
        paintList = new ArrayList<>();
        redoList = new ArrayList<>();
        tiles = new Tile[Field.MAX_ROWS][Field.MAX_COLS];
    }

    public void createTiles() {

        for(int i = 0; i < tiles.length; i++) {
            for(int j = 0; j < tiles[i].length; j++) {

                tiles[i][j] = new Tile(new Rectangle(j*Field.TILE_SIZE + Field.PADDING, i*Field.TILE_SIZE + Field.PADDING, Field.TILE_SIZE, Field.TILE_SIZE), ColorType.BLACK);
                tiles[i][j].rectangle.draw();
                tiles[i][j].isFilled = false;
            }
        }
    }

    public void update() {

        if(paintBrush.keyHandler.painting || paintBrush.keyHandler.cleaning) {
            int brushMinX = paintBrush.brush.getX();
            int brushMaxX = paintBrush.brush.getX()+ paintBrush.brush.getWidth();
            int brushMinY = paintBrush.brush.getY();
            int brushMaxY = paintBrush.brush.getY()+ paintBrush.brush.getHeight();


            for(int i = 0; i < tiles.length; i++) {
                for(int j = 0; j < tiles[i].length; j++) {

                    int tileMinX = tiles[i][j].rectangle.getX();
                    int tileMaxX = tiles[i][j].rectangle.getX() + tiles[i][j].rectangle.getWidth();
                    int tileMinY = tiles[i][j].rectangle.getY();
                    int tileMaxY = tiles[i][j].rectangle.getY() + tiles[i][j].rectangle.getHeight();

                    if((brushMinX <= tileMinX && tileMaxX <= brushMaxX)
                            && (brushMinY <= tileMinY && tileMaxY <= brushMaxY)) {
                        if(keyHandler.painting) {
                            paintList.add(tiles[i][j]);
                            tileList.add(tiles[i][j]);
                            if(keyHandler.blue) tiles[i][j].rectangle.setColor(Color.BLUE);
                            if(keyHandler.magenta) tiles[i][j].rectangle.setColor(Color.MAGENTA);
                            if(keyHandler.yellow) tiles[i][j].rectangle.setColor(Color.YELLOW);
                            if(keyHandler.green) tiles[i][j].rectangle.setColor(Color.GREEN);
                            if(keyHandler.red) tiles[i][j].rectangle.setColor(Color.RED);
                            if(keyHandler.black) tiles[i][j].rectangle.setColor(Color.BLACK);
                            if(keyHandler.randomColor) tiles[i][j].rectangle.setColor(ColorType.GREEN.getRandomColor());

                            tiles[i][j].rectangle.fill();
                            tiles[i][j].isFilled = true;
                        } else if (keyHandler.cleaning){
                            paintList.remove(tiles[i][j]);
                            tileList.remove(tiles[i][j]);
                            //tileList.remove(tiles[i][j]);
                            tiles[i][j].rectangle.setColor(Color.BLACK);
                            tiles[i][j].rectangle.draw();
                            tiles[i][j].isFilled = false;
                        }
                    }
                }
            }

            //keyHandler.painting = false;
        }

        if(paintBrush.keyHandler.saving) {
            save("saves/saveFile2.txt");
            paintBrush.keyHandler.saving = false;
        }

        if(paintBrush.keyHandler.loading) {
            load("saves/saveFile2.txt");
            paintBrush.keyHandler.loading = false;
        }

        /*if(paintBrush.keyHandler.fill) {

        }

         */
    }

    public void clearScreen() {
        for(Tile tile : paintList) {
            tile.rectangle.setColor(Color.BLACK);
            tile.rectangle.draw();
            tile.isFilled = false;
        }
        paintList.clear();
    }

    public void save(String path) {
        try {
            FileWriter fileWriter = new FileWriter(path);
            BufferedWriter bufferedWritter = new BufferedWriter(fileWriter);



            for(int i = 0; i < tiles.length; i++) {
                if(i > 0) {
                    bufferedWritter.write("\n");
                }
                for(int j = 0; j < tiles[i].length; j++) {
                    if(j < tiles[i].length - 1) {

                        if(tiles[i][j].rectangle.getColor() == Color.BLACK){
                            bufferedWritter.write((tiles[i][j].isFilled ? 1 : 0) + " ");
                        } else if (tiles[i][j].rectangle.getColor() == Color.RED) {
                            bufferedWritter.write((tiles[i][j].isFilled ? 2 : 0) + " ");
                        }


                    } else {

                            bufferedWritter.write(tiles[i][j].isFilled ? "1" : "0");

                    }
                }
            }





            /*
            for(Tile tile : tileList) {
                if(tile.rectangle.getColor() == Color.BLACK) {
                    bufferedWritter.write((tile.isFilled ? 1 : 0) + "/" + (tile.rectangle.getX() - Field.PADDING) + "/" + (tile.rectangle.getY() - Field.PADDING));
                } else if(tile.rectangle.getColor() == Color.BLUE) {
                    bufferedWritter.write((tile.isFilled ? 2 : 0) + "/" + (tile.rectangle.getX() - Field.PADDING) + "/" + (tile.rectangle.getY() - Field.PADDING));
                } else if(tile.rectangle.getColor() == Color.MAGENTA) {
                    bufferedWritter.write((tile.isFilled ? 3 : 0) + "/" + (tile.rectangle.getX() - Field.PADDING) + "/" + (tile.rectangle.getY() - Field.PADDING));
                } else if(tile.rectangle.getColor() == Color.YELLOW) {
                    bufferedWritter.write((tile.isFilled ? 4 : 0) + "/" + (tile.rectangle.getX() - Field.PADDING) + "/" + (tile.rectangle.getY() - Field.PADDING));
                } else if(tile.rectangle.getColor() == Color.GREEN) {
                    bufferedWritter.write((tile.isFilled ? 5 : 0) + "/" + (tile.rectangle.getX() - Field.PADDING) + "/" + (tile.rectangle.getY() - Field.PADDING));
                } else if(tile.rectangle.getColor() == Color.RED) {
                    bufferedWritter.write((tile.isFilled ? 6 : 0) + "/" + (tile.rectangle.getX() - Field.PADDING) + "/" + (tile.rectangle.getY() - Field.PADDING));
                }
                bufferedWritter.write("\n");
            }
            tileList.clear();

             */







            bufferedWritter.flush();
            bufferedWritter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    public void load(String path) {
        try{
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);



            String[][] stringArray = new String[Field.MAX_ROWS][Field.MAX_COLS];

            for(int i = 0; i < stringArray.length; i++) {
                stringArray[i] = bufferedReader.readLine().split(" ");
            }

            for(int i = 0; i < tiles.length; i++) {
                for(int j = 0; j < tiles[i].length; j++) {
                    int pos = Integer.parseInt(stringArray[i][j]);
                    if(pos == 0) {
                        tiles[i][j].isFilled = false;
                        tiles[i][j].rectangle.draw();
                    } else if (pos == 1){
                            tiles[i][j].isFilled = true;
                            tiles[i][j].rectangle.setColor(Color.BLACK);
                            tiles[i][j].rectangle.fill();
                    } else if (pos == 2) {
                        tiles[i][j].isFilled = true;
                        tiles[i][j].rectangle.setColor(Color.RED);
                        tiles[i][j].rectangle.fill();
                    }
                }
            }




            /*

            String line = "";
            while((line = bufferedReader.readLine()) != null) {
            String[] tileValues = line.split("/");
                int color = Integer.parseInt(tileValues[0]);
                int x = Integer.parseInt(tileValues[1]);
                int y = Integer.parseInt(tileValues[2]);


                if (color == 1) {
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].isFilled = true;
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.setColor(Color.BLACK);
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.fill();
                } else if (color == 2) {
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].isFilled = true;
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.setColor(Color.BLUE);
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.fill();
                } else if (color == 3) {
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].isFilled = true;
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.setColor(Color.MAGENTA);
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.fill();
                } else if (color == 4) {
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].isFilled = true;
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.setColor(Color.YELLOW);
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.fill();
                } else if (color == 5) {
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].isFilled = true;
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.setColor(Color.GREEN);
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.fill();
                } else if (color == 6) {
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].isFilled = true;
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.setColor(Color.RED);
                    tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE].rectangle.fill();
                }

                paintList.add(tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE]);
                tileList.add(tiles[y/Field.TILE_SIZE][x/Field.TILE_SIZE]);

            }

             */




            bufferedReader.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public Tile[][] getTiles() {
        return tiles;
    }

    public void setPaintBrush(PaintBrush paintBrush) {
        this.paintBrush = paintBrush;
    }
    public void setKeyHandler(KeyHandler keyHandler) {
        this.keyHandler = keyHandler;
    }
}
