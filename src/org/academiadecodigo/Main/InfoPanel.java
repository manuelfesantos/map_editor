package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;

public class InfoPanel {

    Rectangle panel;
    Text moveUp, moveDown, moveLeft, moveRight, paint, clear, grow, shrink, save, load, speedUp, speedDown, blue, magenta, yellow, green, red, black, randomColor;


    public InfoPanel() {
        int x = Field.INFO_PANEL_X + Field.PADDING;
        int y = Field.INFO_PANEL_Y + 16;
        panel = new Rectangle(Field.INFO_PANEL_X, Field.INFO_PANEL_Y, Field.INFO_PANEL_WIDTH, Field.INFO_PANEL_HEIGHT);
        moveUp = new Text(x, y, "W: Move Up");
        moveUp.draw();
        y+= 16;
        moveDown = new Text(x, y, "S: Move Down");
        moveDown.draw();
        y+= 16;
        moveLeft = new Text(x, y, "A: Move Left");
        moveLeft.draw();
        y+= 16;
        moveRight = new Text(x, y, "D: Move Right");
        moveRight.draw();
        y+= 16;
        paint = new Text(x, y, "Space: Toggle Paint Mode");
        paint.draw();
        y+= 16;
        paint = new Text(x, y, "B: Toggle Erase Mode");
        paint.draw();
        y+= 16;
        paint = new Text(x, y, "M: Toggle Move Mode");
        paint.draw();
        y+= 16;
        clear = new Text(x, y, "C: Clear Map");
        clear.draw();
        y+= 16;
        grow = new Text(x, y, "Z: Grow");
        grow.draw();
        y+= 16;
        shrink = new Text(x, y, "X: Shrink");
        shrink.draw();
        y+= 16;
        save = new Text(x, y, "K: Save");
        save.draw();
        y+= 16;
        load = new Text(x, y, "L: Load");
        load.draw();
        y+= 16;
        speedUp = new Text(x, y, "O: Speed Up");
        speedUp.draw();
        y+= 16;
        speedDown = new Text(x, y, "I: Speed Down");
        speedDown.draw();
        y+= 16;
        blue = new Text(x, y, "1: Blue");
        blue.draw();
        y+= 16;
        magenta = new Text(x, y, "2: Magenta");
        magenta.draw();
        y+= 16;
        yellow = new Text(x, y, "3: Yellow");
        yellow.draw();
        y+= 16;
        green = new Text(x, y, "4: Green");
        green.draw();
        y+= 16;
        red = new Text(x, y, "5: Red");
        red.draw();
        y+= 16;
        black = new Text(x, y, "0: Black");
        black.draw();
        y+= 16;
        randomColor = new Text(x, y, "R: Random Color");
        randomColor.draw();
    }

    public void loadInfoPanel() {

    }
}
