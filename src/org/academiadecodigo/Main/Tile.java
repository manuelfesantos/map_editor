package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.w3c.dom.css.Rect;

public class Tile {

    public Rectangle rectangle;
    public boolean isFilled;
    ColorType colorType;

    public Tile(Rectangle rectangle, ColorType colorType) {
        this.rectangle = rectangle;
        rectangle.setColor(colorType.getColor());
    }

    public void setColorType(ColorType colorType) {
        this.colorType = colorType;

    }


}
