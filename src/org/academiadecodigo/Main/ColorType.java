package org.academiadecodigo.Main;

import org.academiadecodigo.simplegraphics.graphics.Color;

public enum ColorType {

    BLUE(Color.BLUE),
    MAGENTA(Color.MAGENTA),
    YELLOW(Color.YELLOW),
    GREEN(Color.GREEN),
    RED(Color.RED),
    BLACK(Color.BLACK);

    private Color color;

    ColorType(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public Color getRandomColor() {
        return values()[(int)(Math.random() * values().length)].color;
    }
}
